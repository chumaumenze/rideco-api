# RideCo Map API Interface

Map API to create map marker, collections and share points of interest

> This work is in progress and could not be completed within allotted time.
> However, the backend API has been completed (except it hasn't been tested). 

![](https://user-images.githubusercontent.com/20656657/131846775-cd23280f-6517-430e-9a41-be118b0ccb85.png)

## Features

- Authentication/Authorization flow using Basic & Bearer
- Validation, Signing and Revoking of JWT token
- Creates, Read, Update and Delete User, Markers & Marker Collections
- Clean and elegant documentation. See [API Docs][backend-api].

## Requirement

- Pip or Poetry
- Python >=3.8.x


### Getting Started

Define the following in your `.env` file in the project root directory.

Defined the following variables
```shell
RIDE_APP_MODE=development
RIDE_MOCK_AUTH=false  # Disable Auth for development
RIDE_DB_DEV_DSN=postgresql://localhost:5432/postgres
```

Additional environment variables can be found [here](/rideco/settings.py)

#### Using Pip

Install dependencies by running
```shell
pip install ./
python ./main.py
```


#### Using Poetry

Install dependencies by running
```shell
poetry env use python
poetry install --no-root
python ./main.py
```

### Scaling Options

- Implement cache layer before database to accommodate large read/write
- Load balance request across multiple servers. 
- And more...

### Links

The application consists of a frontend and a backend.

Application
- [Map app][frontend-web]
- [API Docs][backend-api]

Source code
- [API source code][backend-repo]
- [Frontend source code][frontend-repo]


[frontend-repo]: https://gitlab.com/chumaumenze/rideco-web
[frontend-web]: https://rideco.chumaumenze.com
[backend-repo]: https://gitlab.com/chumaumenze/rideco-api
[backend-api]: https://riderco-map.herokuapp.com/docs