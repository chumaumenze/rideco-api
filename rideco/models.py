import uuid
from datetime import datetime
from typing import Type, Any, Optional, TYPE_CHECKING

from pydantic import BaseModel
from tortoise import Tortoise, fields, Model
from passlib.context import CryptContext

from rideco.constants import MEDIUM_TEXT_LENGTH, LONG_TEXT_LENGTH
from rideco.constants import VERY_LONG_TEXT_LENGTH, VERY_SHORT_TEXT_LENGTH
from rideco.settings import AppMode, settings


if TYPE_CHECKING:
    from pydantic import PostgresDsn

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

DB_DSN: Type["PostgresDsn"] = {
    AppMode.TEST: settings.db_test_dsn,
    AppMode.DEV: settings.db_dev_dsn,
    AppMode.PROD: settings.db_prod_dsn,
}[settings.app_mode]

# TODO: Update DB configuration
DB_CLIENT_CONFIG = {
    "connections": {
        "default": {
            "engine": "tortoise.backends.asyncpg",
            "credentials": {
                "host": DB_DSN.host,
                "port": DB_DSN.port,
                "user": DB_DSN.user,
                "password": DB_DSN.password,
                "database": DB_DSN.path.lstrip("/"),
            },
        },
    },
    "apps": {
        "rideco": {"models": ["rideco.models"]},
    },
    "use_tz": False,
    "timezone": "UTC",
}


async def initialize_database(config: Optional[dict] = None):
    if config is None:
        config = DB_CLIENT_CONFIG
    await Tortoise.init(config=config)
    await Tortoise.generate_schemas()


async def close_database_connections():
    await Tortoise.close_connections()


class BaseMixin(Model):
    id = fields.IntField(pk=True)
    created_at = fields.DatetimeField(null=True, auto_now_add=True)
    modified_at = fields.DatetimeField(null=True)
    created_by = fields.ForeignKeyField(
        "rideco.User", related_name=False, on_delete=fields.SET_NULL, null=True
    )
    modified_by = fields.ForeignKeyField(
        "rideco.User", related_name=False, on_delete=fields.SET_NULL, null=True
    )

    class Meta:
        abstract = True


class DescriptionMixin(BaseMixin):
    name = fields.CharField(max_length=LONG_TEXT_LENGTH, null=True)
    description = fields.TextField(null=True)

    class Meta:
        abstract = True


class User(BaseMixin):
    login_username = fields.CharField(max_length=MEDIUM_TEXT_LENGTH, unique=True)
    login_password = fields.CharField(max_length=MEDIUM_TEXT_LENGTH)
    first_name = fields.CharField(max_length=MEDIUM_TEXT_LENGTH, null=True)
    last_name = fields.CharField(max_length=MEDIUM_TEXT_LENGTH, null=True)

    class PydanticMeta:
        exclude = ["login_password"]

    def set_password(self, plain_password):
        self.login_password = pwd_context.hash(plain_password)

    def verify_password(self, plain_password):
        return pwd_context.verify(plain_password, self.login_password)


class Marker(BaseMixin):
    label = fields.CharField(max_length=LONG_TEXT_LENGTH)
    description = fields.TextField(null=True)
    latitude = fields.DecimalField(max_digits=180, decimal_places=15)
    longitude = fields.DecimalField(max_digits=180, decimal_places=15)

    # collections = fields.ManyToManyRelation["Collection"]
    collection = fields.ManyToManyField("rideco.Collection", related_name="markers")


class Collection(DescriptionMixin):
    uid = fields.CharField(
        max_length=VERY_SHORT_TEXT_LENGTH,
        index=True,
        null=False,
        unique=True,
        default=lambda: uuid.uuid4().hex,
    )
    is_protected = fields.BooleanField(default=True)
    shared_with = fields.ManyToManyField(
        "rideco.User",
        related_name="shared_collections",
        through="users_shared_collection",
    )
    # markers = fields.ManyToManyField(
    #     'rideco.Marker', related_name="collections"
    # )
    markers: fields.ManyToManyRelation["Marker"]


class PublicCollection(DescriptionMixin):
    uid = fields.CharField(
        max_length=VERY_SHORT_TEXT_LENGTH,
        index=True,
        null=False,
        unique=True,
        default=lambda: uuid.uuid4().hex,
    )
    markers = fields.JSONField()


class JWTToken(BaseMixin):
    uid = fields.CharField(
        max_length=VERY_SHORT_TEXT_LENGTH, index=True, null=False, unique=True
    )
    token = fields.CharField(max_length=VERY_LONG_TEXT_LENGTH, null=True)
    issued_to = fields.ForeignKeyField(
        "rideco.User", related_name="tokens", unique=False
    )
    expires_in = fields.DatetimeField()


Tortoise.init_models(app_label="rideco", models_paths=["rideco.models"])


# Request Classes
class UserRequest(BaseModel):
    login_username: str
    login_password: str
    first_name: str
    last_name: str


class MarkerRequest(BaseModel):
    label: str
    description: Optional[str]
    latitude: float
    longitude: float


class CollectionRequest(BaseModel):
    name: Optional[str]
    is_protected: Optional[bool] = False
    description: Optional[str]
    marker_ids: list[int] = []
    shared_with: list[int] = []


# Response Classes
class UserResponse(BaseModel):
    id: int
    login_username: str
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class MarkerResponse(MarkerRequest):
    id: int

    class Config:
        orm_mode = True


class CollectionResponse(BaseModel):
    uid: str
    name: Optional[str]
    is_protected: Optional[bool] = False
    description: Optional[str]
    markers: Optional[list[MarkerResponse]] = []
    shared_with: Optional[list[UserResponse]] = []
    # markers: Optional[Any] = []
    # shared_with: Optional[Any] = []
    # markers: Optional[set[MarkerResponse]]
    # shared_with: Optional[set[UserResponse]]

    class Config:
        orm_mode = True

import typing as t


class PublicCollectionRequest(BaseModel):
    name: str
    description: Optional[str]
    markers: t.Any

    class Config:
        orm_mode = True


class PublicCollectionResponse(PublicCollectionRequest):
    uid: str

    class Config:
        orm_mode = True


class JWTTokenResponse(BaseModel):
    uid: str
    token: str
    expires_in: datetime
    issued_to: UserResponse

    class Config:
        orm_mode = True


__all__ = [
    "User",
    "Marker",
    "Collection",
    "JWTToken",
    "UserRequest",
    "MarkerRequest",
    "CollectionRequest",
    "UserResponse",
    "MarkerResponse",
    "CollectionResponse",
    "JWTTokenResponse",
    "PublicCollection",
    "PublicCollectionRequest",
    "PublicCollectionResponse"
]
