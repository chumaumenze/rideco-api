import os
import uuid
from enum import Enum
from typing import Optional

from pydantic import (
    BaseSettings,
    PostgresDsn,
    FilePath,
)


class AppMode(str, Enum):
    TEST: str = "test"
    DEV: str = "development"
    RC: str = "staging"
    PROD: str = "production"


class LogLevel(Enum):
    CRITICAL: str = "CRITICAL"
    FATAL: str = "FATAL"
    ERROR: str = "ERROR"
    WARNING: str = "WARNING"
    WARN: str = "WARN"
    INFO: str = "INFO"
    DEBUG: str = "DEBUG"
    NOTSET: str = "NOTSET"


class JWTAlgorithm(Enum):
    """
    The HMAC using SHA algorithms are not suggested since you need
    to share secrets between server and client.
    Most OpenID Connect services are using RS256
    """

    HS256: str = "HS256"  # HMAC using SHA-256
    HS384: str = "HS384"  # HMAC using SHA-384
    HS512: str = "HS512"  # HMAC using SHA-512
    # RS256: str = "RS256"  # RSASSA-PKCS1-v1_5 using SHA-256
    # RS384: str = "RS384"  # RSASSA-PKCS1-v1_5 using SHA-384
    # RS512: str = "RS512"  # RSASSA-PKCS1-v1_5 using SHA-512
    # ES256: str = "ES256"  # ECDSA using P-256 and SHA-256
    # ES384: str = "ES384"  # ECDSA using P-384 and SHA-384
    # ES512: str = "ES512"  # ECDSA using P-521 and SHA-512
    # PS256: str = "PS256"  # RSASSA-PSS using SHA-256 and MGF1 with SHA-256
    # PS384: str = "PS384"  # RSASSA-PSS using SHA-384 and MGF1 with SHA-384
    # PS512: str = "PS512"  # RSASSA-PSS using SHA-512 and MGF1 with SHA-512


class Settings(BaseSettings):
    app_name: Optional[str] = "RideCo Map"
    app_description: Optional[str] = "RideCo point of interest map"
    app_mode: AppMode = "production"

    # development
    mock_auth: Optional[bool] = False
    mock_auth_username: Optional[str] = uuid.uuid4().hex

    # Logging
    log_level: Optional[LogLevel] = LogLevel.INFO
    log_file_size: Optional[int] = 10485760  # 10MB
    log_output_filename: Optional[str] = "logs/output.log"
    log_access_filename: Optional[str] = "logs/access.log"

    # Database
    db_test_dsn: Optional[
        PostgresDsn
    ] = "postgresql://zikette:dev@localhost:5432/rideco_test"
    db_dev_dsn: Optional[PostgresDsn] = "postgresql://zikette:dev@localhost:5432/rideco"
    db_prod_dsn: Optional[PostgresDsn] = os.environ.get("DATABASE_URL")

    # JWT Settings
    # JWT expires time, default is 3600
    jwt_token_expires_in: Optional[int] = 3600

    # Default JWT algorithm header
    jwt_alg: Optional[JWTAlgorithm] = JWTAlgorithm.HS256

    # Private key (in text) for JWT
    jwt_private_key: Optional[str] = str(
        "Vai/SaIEltvJnxI7PeU8+zmiQe+sbjbNxsCCt/iDbtCfP5LO+QVRBG4byk483RnN"
        "fITdOpvYGcNkAupfVLF3G562ASg8k6hm4kVFR1sTpiPxWiX3WJdxnvdNg5N4Yma2"
        "H1DDrULDhAryMH2Q54J5bLJ6Kka2LB6MTMX7D+33QNZLc1aIlIwzNP98apVjV4jF"
        "exVutjh9kIKJ259yAVddFLKdSSvxSjZ/1Nrg63sWxsb+Hq1XLJ3JZm0FUNWPdjpV"
        "80XL9Wxwdl78aXuOC5wlDDrA24zXB2wBuICNi7QB1Pbq3mLIe9O0M6+cTlJ+U4Ef"
        "+VdK4YWNGkEudN0zRwdfXQ=="
    )

    class Config:
        env_prefix = "RIDE_"
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
