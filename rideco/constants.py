from pathlib import Path

# TEXT LENGTHS
VERY_TINY_TEXT_LENGTH = 8
TINY_TEXT_LENGTH = 16
VERY_SHORT_TEXT_LENGTH = 32
SHORT_TEXT_LENGTH = 64
MEDIUM_TEXT_LENGTH = 128
LONG_TEXT_LENGTH = 256
VERY_LONG_TEXT_LENGTH = 512

WEB_URL_PATTERN = (
    r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\."
    r"[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
)
PROJECT_DIR = Path(__file__).parent.parent.absolute()
