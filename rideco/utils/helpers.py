import datetime as dt
import typing as t
import uuid
from calendar import timegm
from types import SimpleNamespace

from fastapi import Depends
from fastapi.templating import Jinja2Templates
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jwt import api_jwt as jwt, PyJWTError

from rideco.settings import settings
from rideco.constants import PROJECT_DIR
from rideco.models import User, JWTToken
from rideco.errors import InvalidAuthToken, BadRequest


if t.TYPE_CHECKING:
    from fastapi import Request

template_dir = PROJECT_DIR / "templates"
templates = Jinja2Templates(directory=str(template_dir))


def render_template(filename, request: "Request", /, **context):
    context.update(
        {
            "request": request,
            "app_settings": settings,
        }
    )
    return templates.TemplateResponse(filename, context=context)


async def create_auth_token(issued_to: "User", payload: t.Optional[dict] = None):
    payload = payload or {}
    now = timegm(dt.datetime.utcnow().utctimetuple())
    kid = uuid.uuid4().hex
    expires_in = now + settings.jwt_token_expires_in
    payload.update(
        dict(
            exp=expires_in,
            nbf=now,
            iat=now,
            iss=settings.app_name,
            kid=kid,
            owner_id=issued_to.id,
        )
    )
    token = jwt.encode(
        payload=payload, key=settings.jwt_private_key, algorithm=settings.jwt_alg.value
    )
    token = await JWTToken.create(
        uid=kid, token=token, expires_in=expires_in, issued_to_id=issued_to.id
    )
    await token.fetch_related("issued_to")
    await token.save(force_update=True)
    return token


async def create_mock_token():
    user = await User.get_or_none(login_username=settings.mock_auth_username)
    if user is None:
        user = await User(
            login_username=settings.mock_auth_username,
            first_name='Mock', last_name='User'
        )
        user.set_password(settings.mock_auth_username)
        await user.save()
    rand_str = uuid.uuid4().hex
    token = SimpleNamespace(
        issued_to=user, uid=rand_str,
        token=rand_str, expires_in=3600
    )
    return token


async def require_auth(
    cred: t.Optional[HTTPAuthorizationCredentials] = Depends(
        HTTPBearer(auto_error=False)
    ),
):
    if settings.mock_auth:
        return await create_mock_token()

    if cred is None:
        raise BadRequest("Missing authentication credentials")

    try:
        claims = jwt.decode_complete(
            jwt=cred.credentials,
            key=settings.jwt_private_key,
            algorithms=settings.jwt_alg.value,
        )
    except PyJWTError:
        raise InvalidAuthToken
    kid = claims["payload"].get("kid")
    owner_id = claims["payload"].get("owner_id")
    token = (
        await JWTToken.filter(uid=kid, issued_to__id=owner_id)
        .prefetch_related("issued_to")
        .get_or_none()
    )
    if token is not None and token.issued_to.id == owner_id:
        return token
    raise InvalidAuthToken


async def optional_auth(
    cred: t.Optional[HTTPAuthorizationCredentials] = Depends(
        HTTPBearer(auto_error=False)
    ),
):
    if cred is not None:
        try:
            return await require_auth(cred)
        except (BadRequest, InvalidAuthToken):
            pass
    return None
