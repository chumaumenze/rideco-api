from enum import Enum
from typing import Optional, Union, Any

from pydantic import BaseModel


class ResponseType(Enum):
    """Defines the type of response"""

    SUCCESS: str = "success"
    FAILURE: str = "failure"
    ERROR: str = "error"


# class SuccessResponse(BaseModel):
#     """Signifies a successful request"""
#     status: ResponseType = ResponseType.SUCCESS
#     message: Optional[str] = None
#     data: Optional[Union[list, dict, Any]] = None


class FailureResponse(BaseModel):
    """Signifies a client-side error"""

    status: ResponseType = ResponseType.FAILURE
    message: str


class ErrorResponse(BaseModel):
    """Signifies a server-side error"""

    status: ResponseType = ResponseType.ERROR
    message: str


# def make_success_response(data: Optional[Any] = None) -> SuccessResponse:
#     return SuccessResponse(data=data)


RESPONSES = {
    400: {
        "model": FailureResponse,
        "content": {
            "application/json": {
                "example": {
                    "status": ResponseType.FAILURE,
                    "message": "[ERROR_LOC]: error message here",
                }
            }
        },
    },
    500: {
        "model": ErrorResponse,
        "content": {
            "application/json": {
                "example": {
                    "status": ResponseType.ERROR,
                    "message": "Internal Server Error",
                }
            }
        },
    },
}

__all__ = [
    # "SuccessResponse",
    "FailureResponse",
    "ErrorResponse",
]
