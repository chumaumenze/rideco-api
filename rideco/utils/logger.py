from rideco.settings import settings


LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "()": "uvicorn.logging.DefaultFormatter",
            "fmt": "%(levelprefix)s %(message)s",
            "use_colors": None,
        },
        "access": {
            "()": "uvicorn.logging.AccessFormatter",
            "fmt": '%(levelprefix)s %(client_addr)s - "%(request_line)s" %(status_code)s',  # noqa: E501
        },
        "file": {
            "()": "uvicorn.logging.AccessFormatter",
            "fmt": '%(levelprefix)s %(client_addr)s - "%(request_line)s" %(status_code)s',  # noqa: E501
        },
    },
    "handlers": {
        "default": {
            "class": "logging.StreamHandler",
            # "level": "INFO",
            "formatter": "default",
            "stream": "ext://sys.stderr",
        },
        "default_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": settings.log_level.value,
            "formatter": "default",
            "filename": settings.log_output_filename,
            "maxBytes": 10485760,  # 10MB
            "backupCount": 20,
            "encoding": "utf8",
        },
        "access": {
            "formatter": "access",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
        },
        "access_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": settings.log_level.value,
            "formatter": "access",
            "filename": settings.log_access_filename,
            "maxBytes": "cfg://handlers.default_file.maxBytes",
            "backupCount": "cfg://handlers.default_file.backupCount",
            "encoding": "utf8",
        },
    },
    "root": {"handlers": ["default"], "level": settings.log_level.value},
    "loggers": {
        "uvicorn.error": {
            "handlers": ["default", "default_file"],
            "level": "INFO",
            "propagate": False,
        },
        "uvicorn.asgi": {
            "handlers": ["default", "default_file"],
            "level": "INFO",
            "propagate": False,
        },
        "uvicorn.access": {
            "handlers": ["access", "access_file"],
            "level": "INFO",
            "propagate": False,
        },
        # TODO: Customize Fast API & Gunicorn loggers
        "fastapi": {"handlers": ["default", "default_file"], "propagate": False},
        "gunicorn.error": "cfg://loggers.fastapi",
        "gunicorn.access": "cfg://loggers.fastapi",
    },
}
