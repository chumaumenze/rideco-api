from fastapi import APIRouter, Depends, Request
from fastapi.responses import RedirectResponse
from tortoise.query_utils import Q

from rideco.models import *
from rideco.errors import BadRequest, MissingRequired, NotFound
from rideco.utils.helpers import require_auth, optional_auth

router = APIRouter(prefix="/collections", tags=["Collections"])

# TODO: DRY out repetitions
@router.post("/", response_model=CollectionResponse)
async def create_collection(
    data: CollectionRequest, token: "JWTToken" = Depends(require_auth)
):
    if not data.name:
        raise MissingRequired
    marker_ids = set(data.marker_ids)
    shared_with = set(data.shared_with)
    if token.issued_to.id in shared_with:
        shared_with.remove(token.issued_to.id)
    markers = await Marker.filter(
        created_by_id=token.issued_to.id, id__in=marker_ids
    ).all()
    shared_users = await User.filter(id__in=shared_with).all()
    collection = await Collection.create(
        created_by_id=token.issued_to.id,
        **data.dict(exclude={"marker_ids", "shared_with"})
    )
    await collection.markers.add(*markers)
    await collection.shared_with.add(*shared_users)
    # shared_users = await collection.shared_with.all()
    # markers = await collection.markers.all()
    return CollectionResponse(
        uid=collection.uid,
        name=collection.name,
        is_protected=collection.is_protected,
        description=collection.description,
        markers=markers,
        shared_with=shared_users,
    )


@router.get("/", response_model=list[CollectionResponse])
async def get_all_collections(token: "JWTToken" = Depends(require_auth)):
    collections = (
        await Collection.filter(created_by_id=token.issued_to.id)
        .all()
        .prefetch_related("shared_with", "markers")
        .all()
    )
    rv = []
    for c in collections:
        users = await collections[0].shared_with.all()
        markers = await collections[0].markers.all()
        c = CollectionResponse(
            uid=c.uid,
            name=c.name,
            is_protected=c.is_protected,
            description=c.description,
            markers=markers,
            shared_with=users,
        )
        rv.append(c)
    return rv


@router.get("/{collection_uid}", response_model=CollectionResponse)
async def get_collections(
    collection_uid: str, token: "JWTToken" = Depends(optional_auth)
):
    collection = await Collection.get_or_none(uid=collection_uid)
    if collection is None:
        raise NotFound
    if collection.is_protected and token is None:
        raise BadRequest("Missing authentication credentials")
    if collection.is_protected and token:
        collection = await collection.filter(
            Q(
                created_by_id=token.issued_to.id,
                join_type="OR",
                shared_with__id=token.issued_to.id,
            ),
            uid=collection_uid,
        ).get_or_none()

    users = await collection.shared_with.all()
    markers = await collection.markers.all()
    return CollectionResponse(
        uid=collection.uid,
        name=collection.name,
        is_protected=collection.is_protected,
        description=collection.description,
        markers=markers,
        shared_with=users,
    )


# @router.put('/{collection_id}', response_model=CollectionResponse)
# async def update_collection(collection_id: str, data: CollectionRequest, token: "JWTToken" = Depends(require_auth)):
#     collection = await Collection.get_or_none(
#         created_by_id=token.issued_to.id, uid=collection_id)
#     if collection is None:
#         raise NotFound
#
#     await collection.update_from_dict(data.dict(exclude={'marker_ids', 'shared_with'}))
#     markers = await Marker.filter(
#         created_by_id=token.issued_to.id, id__in=set(data.marker_ids)).all()
#     shared_users = await User.filter(
#         created_by_id=token.issued_to.id, id__in=set(data.shared_with)).all()
#     await collection.markers.add(*markers)
#     await collection.shared_with.add(*shared_users)
#     await collection.save()
#     return CollectionResponse(
#         uid=collection.id, name=collection.name,
#         is_protected=collection.is_protected,
#         description=collection.description,
#         markers=markers, shared_with=shared_users,
#     )


@router.delete("/{collection_id}", response_model=CollectionResponse)
async def delete_collections(
    collection_id: str, token: "JWTToken" = Depends(require_auth)
):
    collection = await Collection.get_or_none(
        created_by__id=token.issued_to.id, uid=collection_id
    )
    if collection is None:
        raise NotFound
    return await collection.delete()
