from fastapi import APIRouter, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from rideco.errors import UnsuccessfulAuthentication, BadRequest
from rideco.models import *
from rideco.utils.helpers import create_auth_token, require_auth

router = APIRouter(prefix="/auth", tags=["Authentication"])


@router.get("/login", response_model=JWTTokenResponse)
async def get_auth_token(
    credentials: HTTPBasicCredentials = Depends(HTTPBasic(auto_error=False)),
):
    if credentials is None:
        raise BadRequest("Missing authentication credentials")
    user = await User.get_or_none(login_username=credentials.username)
    if user is not None and user.verify_password(credentials.password):
        token = await create_auth_token(user)
        return JWTTokenResponse.from_orm(token)
    raise UnsuccessfulAuthentication


@router.get("/logout")
async def revoke_auth_token(token: "JWTToken" = Depends(require_auth)):
    await token.delete()
    return
