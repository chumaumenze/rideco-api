from fastapi import APIRouter, Depends, Request
from fastapi.responses import RedirectResponse
from tortoise.exceptions import IntegrityError

from rideco.models import *
from rideco.errors import BadRequest, MissingRequired
from rideco.utils.helpers import require_auth

router = APIRouter(prefix="/users", tags=["Users"])


@router.post("", response_model=UserResponse)
async def create_user(data: UserRequest):
    if not all([data.login_username, data.login_password]):
        raise MissingRequired
    user = await User(
        login_username=data.login_username,
        first_name=data.first_name,
        last_name=data.last_name,
    )
    user.set_password(data.login_password)
    try:
        await user.save()
    except IntegrityError:
        raise BadRequest(message="Username is not available")
    return UserResponse.from_orm(user)


@router.get("/markers")
async def get_user_markers(req: Request, _: "JWTToken" = Depends(require_auth)):
    return RedirectResponse("/api/markers", headers=dict(req.headers))


@router.get("/collections")
async def get_user_collections(req: Request, _: "JWTToken" = Depends(require_auth)):
    return RedirectResponse("/api/collections", headers=dict(req.headers))
