from fastapi import APIRouter

from .users import router as user_router
from .auth import router as auth_router
from .markers import router as markers_router
from .collections import router as collection_router
from .publiccollections import router as public_collection_router

api = APIRouter()
api.include_router(auth_router)
api.include_router(user_router)
api.include_router(markers_router)
api.include_router(collection_router)
api.include_router(public_collection_router)


@api.get('/')
def welcome():
    return {"status": "success", "message": "Welcome to RideCo"}
