from fastapi import APIRouter, Depends, Request
from fastapi.responses import RedirectResponse

from rideco.models import *
from rideco.errors import BadRequest, MissingRequired, NotFound
from rideco.utils.helpers import require_auth

router = APIRouter(prefix="/markers", tags=["Markers"])


@router.post("/", response_model=MarkerResponse)
async def create_marker(data: MarkerRequest, token: "JWTToken" = Depends(require_auth)):
    if not all(data.dict().values()):
        raise MissingRequired
    marker = await Marker.create(created_by_id=token.issued_to.id, **data.dict())
    return MarkerResponse.from_orm(marker)


@router.get("/{marker_id}", response_model=MarkerResponse)
async def get_markers(marker_id: int, token: "JWTToken" = Depends(require_auth)):
    marker = await Marker.get_or_none(created_by_id=token.issued_to.id, id=marker_id)
    if marker is None:
        raise NotFound
    return MarkerResponse.from_orm(marker)


@router.get("/", response_model=list[MarkerResponse])
async def get_all_markers(token: "JWTToken" = Depends(require_auth)):
    markers = await Marker.filter(created_by_id=token.issued_to.id).all()
    return [MarkerResponse.from_orm(m) for m in markers]


@router.put("/{marker_id}", response_model=MarkerResponse)
async def update_marker(
    marker_id: int, data: MarkerRequest, token: "JWTToken" = Depends(require_auth)
):
    marker = await Marker.get_or_none(created_by_id=token.issued_to.id, id=marker_id)
    if marker is None:
        raise NotFound

    await marker.update_from_dict(data.dict())
    await marker.save()
    return MarkerResponse.from_orm(marker)


@router.delete("/{marker_id}", response_model=MarkerResponse)
async def delete_markers(marker_id: int, token: "JWTToken" = Depends(require_auth)):
    marker = await Marker.get_or_none(created_by_id=token.issued_to.id, id=marker_id)
    if marker is None:
        raise NotFound

    return await marker.delete()
