from fastapi import APIRouter

from rideco.models import *
from rideco.errors import BadRequest, MissingRequired, NotFound

router = APIRouter(prefix="/public", tags=["Public Collections"])


@router.post("/", response_model=PublicCollectionResponse)
async def create_collection(data: PublicCollectionRequest):
    if not all(data.dict(exclude={'description'}).values()):
        raise MissingRequired
    collection = await PublicCollection.create(**data.dict())
    return PublicCollectionResponse.from_orm(collection)


@router.get("/{collection_uid}", response_model=PublicCollectionResponse)
async def get_collections(collection_uid: str):
    collection = await PublicCollection.get_or_none(uid=collection_uid)
    if collection is None:
        raise NotFound
    return PublicCollectionResponse.from_orm(collection)
