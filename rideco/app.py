import importlib

from fastapi import FastAPI, Request, Response
from fastapi.exceptions import (
    StarletteHTTPException,
    RequestValidationError,
    WebSocketRequestValidationError,
)
from fastapi.logger import logger
from fastapi.staticfiles import StaticFiles
from pydantic.error_wrappers import ValidationError
from starlette.middleware.cors import CORSMiddleware

from rideco.routes import api_router
from tortoise.exceptions import BaseORMException

from rideco.errors import APIException, exc_handler
from rideco.models import initialize_database, close_database_connections

app = FastAPI(title="RideCo Map", description="RideCo point of interest map")

app.include_router(api_router)

# Add Exception Handlers
app.exception_handlers.update(
    {
        Exception: exc_handler,
        APIException: exc_handler,
        StarletteHTTPException: exc_handler,
        ValidationError: exc_handler,
        BaseORMException: exc_handler,
    }
)

# Add CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.middleware_stack = app.build_middleware_stack()


@app.on_event("startup")
async def startup_event():
    logger.info("➡️ Initializing database.")
    await initialize_database()
    logger.info("✅ Database initialized.")


@app.on_event("shutdown")
async def shutdown_event():
    logger.info("➡️ Terminating database connections.")
    await close_database_connections()
    logger.info("✅ Database connections terminated.")
