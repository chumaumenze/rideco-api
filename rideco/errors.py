from __future__ import annotations

import traceback
from typing import Optional, Union, TYPE_CHECKING

from pydantic.error_wrappers import ValidationError
from fastapi.responses import JSONResponse
from fastapi.logger import logger

from rideco.utils.responses import ResponseType

if TYPE_CHECKING:
    from fastapi import Request
    from fastapi.exceptions import StarletteHTTPException
    from tortoise.exceptions import BaseORMException


class APIException(Exception):
    code: int = 500
    message: Optional[str] = "Internal Server Error"

    def __init__(self, message: Optional[str] = None, code: Optional[int] = None):
        self.code = code or self.code
        self.message = message or self.message


class BadRequest(APIException):
    code = 400
    message = "Bad Request"


class MissingRequired(APIException):
    code = 400
    message = "Missing required field"


class InvalidAuthToken(APIException):
    code = 498
    message = "Invalid auth token"


class Unauthorized(APIException):
    code = 401
    message = "Unauthorized access"


class NotFound(APIException):
    code = 404
    message = "Not found"


class UnsuccessfulAuthentication(Unauthorized):
    message = "Unsuccessful authentication"


def exc_handler(
    r: "Request",
    e: Exception,
):
    logger.debug(f"Handling exception: [{type(e).__name__}] {e}")
    traceback.print_exception(type(e), e, e.__traceback__)
    return handle_api_exc(r, e)


def get_exc_attr(e):
    default_err_code = 500
    meta = None
    message = None
    if isinstance(e, ValidationError):
        code = 400
        message = "The request is malformed"
        meta = e.errors()
    else:
        code = getattr(e, "status_code", getattr(e, "code", default_err_code))
        msgtable = {"5": APIException.message, '4': "Client error - Bad request"}
        message = getattr(e, "message", None)
        if message is None:
            message = msgtable[str(code)[0]]

    exc_type = ResponseType.FAILURE if str(code).startswith("4") else ResponseType.ERROR

    return dict(status=exc_type.value, message=message, code=code, meta=meta)


def handle_api_exc(_, e):
    exc_attr = get_exc_attr(e)
    response = {
        "status": exc_attr["status"],
        "message": exc_attr["message"],
        "meta": exc_attr["meta"],
    }
    return JSONResponse(content=response, status_code=exc_attr["code"])
