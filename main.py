from rideco.app import app
from rideco.utils.logger import LOGGING_CONFIG


if __name__ == "__main__":
    import uvicorn

    # TODO: Include addition options for different environments
    uvicorn.run(app, log_config=LOGGING_CONFIG)
